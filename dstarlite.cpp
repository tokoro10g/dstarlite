////
// D* Lite implementation in C++
// @tokoro10g
// Reference: http://idm-lab.org/bib/abstracts/papers/aaai02b.pdf
////

#include <bits/stdc++.h>
#include <chrono>

using namespace std;
using namespace std::chrono;

union __attribute__((__packed__)) Direction {
    uint8_t half : 4;
    struct __attribute__((__packed__)) {
        unsigned NORTH : 1;
        unsigned EAST : 1;
        unsigned SOUTH : 1;
        unsigned WEST : 1;
    } bits;
};

union __attribute__((__packed__)) CellData {
    uint8_t byte : 8;
    struct __attribute__((__packed__)) {
        unsigned NORTH : 1;
        unsigned EAST : 1;
        unsigned SOUTH : 1;
        unsigned WEST : 1;
        unsigned CHECKED_NORTH : 1;
        unsigned CHECKED_EAST : 1;
        unsigned CHECKED_SOUTH : 1;
        unsigned CHECKED_WEST : 1;
    } bits;
};

struct __attribute__((__packed__)) Coord {
    uint8_t x;
    uint8_t y;
    Direction dir;
};

typedef std::vector<CellData> MazeData;

class Maze {
private:
    MazeData data;
    uint8_t w;
    uint8_t h;
    uint8_t type;
    uint8_t gx, gy;
    Direction gd;

    void setWallInternal(Coord c, Direction dir, bool is_checked, bool val)
    {
        data[c.y * w + c.x].byte |= (dir.half << (4 * is_checked));

        if (dir.bits.SOUTH && c.y != 0) {
            data[(c.y - 1) * w + c.x].bits.NORTH = val;
        }
        if (dir.bits.EAST && c.x != w - 1) {
            data[c.y * w + c.x + 1].bits.WEST = val;
        }
        if (dir.bits.NORTH && c.y != w - 1) {
            data[(c.y + 1) * w + c.x].bits.SOUTH = val;
        }
        if (dir.bits.WEST && c.x != 0) {
            data[c.y * w + c.x - 1].bits.EAST = val;
        }
    }

public:
    static constexpr Direction DirFront = { 0x1 };
    static constexpr Direction DirRight = { 0x2 };
    static constexpr Direction DirBack = { 0x4 };
    static constexpr Direction DirLeft = { 0x8 };
    static constexpr Direction DirNorth = { 0x1 };
    static constexpr Direction DirEast = { 0x2 };
    static constexpr Direction DirSouth = { 0x4 };
    static constexpr Direction DirWest = { 0x8 };

public:
    Maze()
        : w(0)
        , h(0)
        , type(0)
        , gx(0)
        , gy(0)
    {
        gd.half = 0x2;
    }
    Maze(uint8_t _w, uint8_t _h)
        : w(_w)
        , h(_h)
        , type(0)
        , gx(0)
        , gy(0)
    {
        gd.half = 0x2;
    }
    Maze(const Maze& m)
        : data(m.data)
        , w(m.w)
        , h(m.h)
        , type(m.type)
        , gx(m.gx)
        , gy(m.gy)
    {
        gd.half = 0x2;
    }
    ~Maze() { data.clear(); }

    uint8_t getWidth() const { return w; }
    uint8_t getHeight() const { return h; }
    void resize(uint8_t _w, uint8_t _h)
    {
        w = _w;
        h = _h;
        data.resize(w * h);
    }

    void setType(uint8_t _type) { type = _type; }
    uint8_t getGoalX() const { return gx; }
    uint8_t getGoalY() const { return gy; }
    Direction getGoalDir() const { return gd; }
    void setGoal(uint8_t _gx, uint8_t _gy)
    {
        gx = _gx;
        gy = _gy;
    }
    void setGoalDir(Direction d) { gd = d; }

    void setWall(Coord c, Direction dir) { setWallInternal(c, dir, false, true); }
    void setCheckedWall(Coord c, Direction dir) { setWallInternal(c, dir, true, true); }
    void resetWall(Coord c, Direction dir) { setWallInternal(c, dir, false, false); }
    void resetCheckedWall(Coord c, Direction dir) { setWallInternal(c, dir, true, false); }
    void toggleWall(Coord c, Direction dir)
    {
        if (isSetWall(c, dir)) {
            resetWall(c, dir);
        } else {
            setWall(c, dir);
        }
    }
    void toggleCheckedWall(Coord c, Direction dir)
    {
        if (isCheckedWall(c, dir)) {
            resetCheckedWall(c, dir);
        } else {
            setCheckedWall(c, dir);
        }
    }
    void setCell(uint16_t index, CellData cd) { data[index] = cd; }
    CellData getCell(uint16_t index) const { return data[index]; }

    bool isSetWall(Coord c, Direction dir) const { return isSetWall(c.y * w + c.x, dir); }
    bool isSetWall(uint16_t index, Direction dir) const { return data[index].byte & dir.half; }
    bool isCheckedWall(Coord c, Direction dir) const { return data[c.y * w + c.x].byte & (dir.half << 4); }
    bool isCheckedCell(Coord c) const { return isCheckedCell(c.y * w + c.x); }
    bool isCheckedCell(uint16_t index) const { return (data[index].byte & 0xF0) == 0xF0; }
};

void loadMaze(Maze& maze)
{
    int type, w, h;
    cin >> type;
    cin >> w >> h;
    maze.resize(w, h);
    maze.setType(type);
    if (type == 1) {
        int x, y;
        cin >> x >> y;
        maze.setGoal(x, y);
    } else {
        maze.setGoal(7, 7);
    }
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            char ch;
            CellData cell = { 0 };
            cin >> ch;
            if (ch >= '0' && ch <= '9') {
                cell.byte = ch - '0';
            } else if (ch >= 'a' && ch <= 'f') {
                cell.byte = ch + 0xa - 'a';
            } else if (ch == ' ' || ch == '\n' || ch == '\r') {
                j--;
                continue;
            } else {
                cerr << "Invalid maze data" << std::endl;
                return;
            }
            maze.setCell((h - i - 1) * w + j, cell);
        }
    }
}

void loadEmptyMaze(int w, int h, int x, int y, Maze& maze)
{
    maze.resize(w, h);
    maze.setType(1);
    maze.setGoal(x, y);
    for (int i = 0; i < h; i++) {
        for (int j = 0; j < w; j++) {
            CellData cell = { 0 };
            if (j == 0) {
                cell.bits.WEST = 1;
            } else if (j == w - 1) {
                cell.bits.EAST = 1;
            }
            if (i == 0) {
                cell.bits.SOUTH = 1;
            } else if (i == h - 1) {
                cell.bits.NORTH = 1;
            }
            if (i == 0 && j == 0) {
                cell.bits.EAST = 1;
                cell.bits.CHECKED_EAST = 1;
            }
            if (i == 0 && j == 1) {
                cell.bits.WEST = 1;
                cell.bits.CHECKED_WEST = 1;
            }
            maze.setCell(i * w + j, cell);
        }
    }
}

void showMaze(Maze& maze)
{
    int w = maze.getWidth();
    int h = maze.getHeight();

    std::cout << "_";
    for (int i = 0; i < w * 3; i++) {
        std::cout << "__";
    }
    std::cout << std::endl;

    for (int cursory = h - 1; cursory >= 0; cursory--) {
        for (int j = 0; j < 3; j++) {
            for (int cursorx = 0; cursorx < w; cursorx++) {
                int index = cursorx + w * cursory;
                CellData cd = maze.getCell(index);
                if (cursorx == 0)
                    std::cout << "|";

                for (int i = 0; i < 3; i++) {
                    if (cd.bits.SOUTH && j == 2) {
                        std::cout << "\x1b[4m"; // underline
                    }
                    std::cout << " ";
                    std::cout << "\x1b[0m";
                }

                for (int i = 0; i < 3; i++) {
                    if (cd.bits.EAST && i == 2) {
                        std::cout << "|";
                    } else if (cd.bits.SOUTH && j == 2) {
                        std::cout << "\x1b[4m";
                        std::cout << " ";
                        std::cout << "\x1b[0m";
                    } else {
                        std::cout << " ";
                    }
                }
                if (cursorx == w - 1) {
                    std::cout << std::endl;
                }
            }
        }
    }
}

//using Cost = float;
//constexpr Cost inf = numeric_limits<Cost>::infinity();
//constexpr Cost large = inf;

using Cost = uint8_t;
constexpr Cost inf = numeric_limits<Cost>::max();
constexpr Cost large = numeric_limits<Cost>::max();

template <typename T>
T satSum(T a, T b)
{
    if (numeric_limits<T>::max() - a <= b) {
        return numeric_limits<T>::max();
    } else {
        return a + b;
    }
}

Cost key_modifier;

using NodeId = uint16_t;
using Edge = pair<NodeId, NodeId>;
struct EdgeHasher {
    size_t operator()(const Edge& e) const
    {
        return ((uint32_t)e.first << 16) | ((uint32_t)e.second);
    }
};
using HeapKey = pair<Cost, Cost>;

vector<pair<HeapKey, NodeId>> open_list;
int in_open_list[10000];

NodeId id_start_orig = 0;
NodeId id_start;
NodeId id_goal = 1;
NodeId id_last;

constexpr int given_maze_size = 32;

struct KeyCompare {
    bool operator()(const pair<HeapKey, NodeId>& a, const pair<HeapKey, NodeId>& b) const
    {
        return a.first > b.first;
    }
};

Cost heuristic(int id1, int id2)
{
    const int w = given_maze_size;
    int x1 = id1 % w, y1 = id1 / w;
    int x2 = id2 % w, y2 = id2 / w;
    return max(abs(x1 - x2), abs(y1 - y2));
    //return sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
}

class Node {
public:
    Node(const int id)
        : id(id)
        , neighbors()
        , g(inf)
        , rhs(inf) {};
    const NodeId id;
    vector<Node*> neighbors;
    Cost g;
    Cost rhs;

    const HeapKey calculateKey() const
    {
        return { satSum(satSum(min(g, rhs), heuristic(id_start, id)), key_modifier), min(g, rhs) };
    }
};

class Graph {
public:
    Graph()
        : size(0)
    {
    }
    Graph(unsigned int size)
        : size(size)
    {
        assert(size > 2);
        for (NodeId i = 0; i < size; i++) {
            nodes.push_back(new Node(i));
        }
    }
    ~Graph()
    {
        for (NodeId i = 0; i < size; i++) {
            delete nodes[i];
        }
    }
    void connect(NodeId id1, NodeId id2, Cost cost)
    {
        assert(id1 < size && id2 < size);
        // undirected graph
        nodes[id1]->neighbors.push_back(nodes[id2]);
        nodes[id2]->neighbors.push_back(nodes[id1]);
        edges.insert({ makeEdgeKey(id1, id2), cost });
    }
    const bool edgeExists(NodeId id1, NodeId id2) const
    {
        assert(id1 < size && id2 < size);
        return edges.find(makeEdgeKey(id1, id2)) != edges.end();
    }
    const Cost getEdgeCost(NodeId id1, NodeId id2) const
    {
        if (id1 == id2)
            return 0;
        assert(id1 < size && id2 < size);
        assert(edgeExists(id1, id2));
        return edges.at(makeEdgeKey(id1, id2));
    }
    void setEdgeCost(NodeId id1, NodeId id2, Cost cost)
    {
        assert(id1 < size && id2 < size);
        assert(edgeExists(id1, id2));
        edges.at(makeEdgeKey(id1, id2)) = cost;
    }
    Node* getNode(NodeId id)
    {
        assert(id < size);
        return nodes[id];
    }
    void setNodeG(NodeId id, Cost g)
    {
        assert(id < size);
        nodes[id]->g = g;
    }
    void setNodeRhs(NodeId id, Cost rhs)
    {
        assert(id < size);
        nodes[id]->rhs = rhs;
    }

    static const Edge makeEdgeKey(NodeId id1, NodeId id2)
    {
        return { min(id1, id2), max(id1, id2) };
    }

public:
    const unsigned int size;

private:
    vector<Node*> nodes;
    unordered_map<Edge, Cost, EdgeHasher> edges;
};

Graph graph(given_maze_size* given_maze_size);

ostream& operator<<(ostream& os, pair<Cost, Cost> const& p)
{
    return os << "<" << (int)p.first << ", " << (int)p.second << ">";
}

ostream& operator<<(ostream& os, Node const& n)
{
    return os << n.id << make_pair(n.g, n.rhs);
}

ostream& operator<<(ostream& os, Graph& g)
{
    for (NodeId i = 0; i < g.size; i++) {
        Node* n = g.getNode(i);
        os << *n << ": ";
        for (auto nn : n->neighbors) {
            os << nn->id << "(" << (int)g.getEdgeCost(nn->id, i) << "|" << (int)satSum(nn->g, g.getEdgeCost(nn->id, i)) << ") ";
        }
        os << endl;
    }
    return os;
}

void updateHeap(NodeId id, HeapKey k)
{
    for (auto& p : open_list) {
        if (p.second == id) {
            p.first = k;
            return;
        }
    }
}

void insertHeap(NodeId id, HeapKey k)
{
    open_list.push_back({ k, id });
    push_heap(open_list.begin(), open_list.end(), KeyCompare());
    in_open_list[id]++;
}

void updateVertex(NodeId id)
{
    Node* n = graph.getNode(id);
    if (n->g != n->rhs && in_open_list[id]) {
        updateHeap(id, n->calculateKey());
    } else if (n->g != n->rhs && !in_open_list[id]) {
        insertHeap(id, n->calculateKey());
    } else if (n->g == n->rhs && in_open_list[id]) {
        auto it = find_if(open_list.begin(), open_list.end(), [=](auto p) { return p.second == id; });
        open_list.erase(it);
        in_open_list[id]--;
    }
}

void computeShortestPath()
{
    int examined_nodes = 0;
    int max_heap_size = 0;
    Node* n = graph.getNode(id_start);
    while (!open_list.empty() && (open_list.front().first < n->calculateKey() || n->rhs > n->g)) {
        auto uid = open_list.front().second;
        Node* u = graph.getNode(uid);
        auto kold = open_list.front().first;
        auto knew = u->calculateKey();
        examined_nodes++;
        if (kold < knew) {
            updateHeap(u->id, knew);
        } else if (u->g > u->rhs) {
            u->g = u->rhs;
            pop_heap(open_list.begin(), open_list.end(), KeyCompare());
            open_list.pop_back();
            in_open_list[u->id]--;
            for (auto s : u->neighbors) {
                if (s->rhs != 0) {
                    s->rhs = min(s->rhs, satSum(graph.getEdgeCost(u->id, s->id), u->g));
                }
                updateVertex(s->id);
            }
        } else {
            Cost gold = u->g;
            u->g = inf;
            auto f = [=](Node* s) {
                if (s->rhs == satSum(graph.getEdgeCost(s->id, u->id), gold)) {
                    if (s->rhs != 0) {
                        Cost mincost = inf;
                        for (auto sp : s->neighbors) {
                            mincost = min(mincost, satSum(graph.getEdgeCost(sp->id, s->id), sp->g));
                        }
                        s->rhs = mincost;
                    }
                }
                updateVertex(s->id);
            };
            for_each(u->neighbors.begin(), u->neighbors.end(), f);
            f(u);
        }
        make_heap(open_list.begin(), open_list.end(), KeyCompare());
        if (open_list.size() > max_heap_size) {
            max_heap_size = open_list.size();
        }
    }
    cout << "The number of examined nodes in this round: " << examined_nodes << endl;
    cout << "Maximum size of the open list: " << max_heap_size << endl;
}

int main(int argc, char* argv[])
{

    int move_from = -1, move_to = -2;
    int iloop_count = 0;
    int nsteps = 0;

    Maze maze;
    loadMaze(maze);
    int maze_width = maze.getWidth();
    int maze_height = maze.getHeight();
    int maze_goal_x = maze.getGoalX();
    int maze_goal_y = maze.getGoalY();

    showMaze(maze);

    for (int cursory = 0; cursory < maze_height; cursory++) {
        for (int cursorx = 0; cursorx < maze_width; cursorx++) {
            NodeId id = cursorx + given_maze_size * cursory;
            CellData cell = maze.getCell(id);
            if (cursorx != maze_width - 1) {
                if (cursorx != 0 || cursory != 0) {
                    graph.connect(id, id + 1, 1);
                }
            }
            if (cursory != maze_height - 1) {
                graph.connect(id, id + given_maze_size, 1);
            }
        }
    }

    //cout << graph << endl;

    high_resolution_clock::time_point tstart = high_resolution_clock::now();

    //////////////////////////
    key_modifier = 0;
    id_start = id_start_orig;
    id_last = id_start;
    id_goal = maze_goal_x + given_maze_size * maze_goal_y;

    cout << "The goal is: " << endl;

    const int goal_width = 1;
    for (int i = 0; i < goal_width; i++) {
        for (int j = 0; j < goal_width; j++) {
            NodeId id = id_goal + i * given_maze_size + j;
            graph.setNodeRhs(id, 0);
            insertHeap(id, { heuristic(id_start, id), 0 });
            cout << id << ", ";
        }
    }
    cout << endl;

    computeShortestPath();

    while (graph.getNode(id_start)->rhs != 0) {
        high_resolution_clock::time_point t0 = high_resolution_clock::now();
        //cout << graph << endl;
        Node* start = graph.getNode(id_start);
        if (start->rhs == inf) {
            cerr << "NO ROUTE!!!" << endl;
            return -1;
        }

        NodeId argmin = id_start;
        Cost mincost = inf;
        for (auto sp : start->neighbors) {
            Cost cost = satSum(graph.getEdgeCost(id_start, sp->id), sp->g);
            //cout << "Cost from " << id_start << " to " << sp->id << ": " << cost << endl;
            if (mincost > cost) {
                mincost = cost;
                argmin = sp->id;
            }
        }
        if (move_from == argmin && move_to == id_start) {
            iloop_count++;
        } else {
            iloop_count = 0;
        }
        if (iloop_count > 2) {
            cerr << "CAUGHT IN AN INFINITE LOOP AT " << argmin << "," << id_start << "!!!!" << endl;
            break;
        }

        nsteps++;
        cout << "Move from " << id_start << " to " << argmin << endl;
        move_from = id_start;
        move_to = argmin;
        id_start = argmin;
        Node* start_new = graph.getNode(id_start);

        bool cost_changed = false;
        vector<Edge> changed_edges;
        for (auto sp : start_new->neighbors) {
            Edge edge = Graph::makeEdgeKey(id_start, sp->id);

            int diff = edge.second - edge.first;
            Coord coord;
            coord.x = edge.first % given_maze_size;
            coord.y = edge.first / given_maze_size;
            Direction dir;
            if (diff == given_maze_size) {
                dir = Maze::DirNorth;
            } else if (diff == -given_maze_size) {
                dir = Maze::DirSouth;
            } else if (diff == 1) {
                dir = Maze::DirEast;
            } else if (diff == -1) {
                dir = Maze::DirWest;
            }
            if (!maze.isCheckedWall(coord, dir) && maze.isSetWall(coord, dir)) {
                maze.setCheckedWall(coord, dir);
                changed_edges.push_back(Graph::makeEdgeKey(id_start, sp->id));
                cost_changed = true;
                cout << id_start << " to " << sp->id << ": " << (maze.isSetWall(coord, dir) ? "wall" : "isle") << endl;
            }
        }
        if (cost_changed) {
            key_modifier += heuristic(id_last, id_start);
            id_last = id_start;
            for (auto e : changed_edges) {
                int diff = e.second - e.first;
                Coord coord;
                coord.x = e.first % given_maze_size;
                coord.y = e.first / given_maze_size;
                Direction dir;
                if (diff == given_maze_size) {
                    dir = Maze::DirNorth;
                } else if (diff == -given_maze_size) {
                    dir = Maze::DirSouth;
                } else if (diff == 1) {
                    dir = Maze::DirEast;
                } else if (diff == -1) {
                    dir = Maze::DirWest;
                }
                Cost cold = graph.getEdgeCost(e.first, e.second);
                graph.setEdgeCost(e.first, e.second, maze.isSetWall(coord, dir) ? large : 1);
                auto u = graph.getNode(e.first);
                auto v = graph.getNode(e.second);
                if (cold > large) {
                    if (u->rhs != 0) {
                        u->rhs = min(u->rhs, satSum(large, v->g));
                    }
                } else if (u->rhs == satSum(cold, v->g)) {
                    if (u->rhs != 0) {
                        Cost mincost = inf;
                        for (auto sp : u->neighbors) {
                            mincost = min(mincost, satSum(graph.getEdgeCost(sp->id, e.first), sp->g));
                        }
                        u->rhs = mincost;
                    }
                }
                updateVertex(e.first);
                if (cold > large) {
                    if (v->rhs != 0) {
                        v->rhs = min(v->rhs, satSum(large, u->g));
                    }
                } else if (v->rhs == satSum(cold, u->g)) {
                    if (v->rhs != 0) {
                        Cost mincost = inf;
                        for (auto sp : v->neighbors) {
                            mincost = min(mincost, satSum(graph.getEdgeCost(sp->id, e.second), sp->g));
                        }
                        v->rhs = mincost;
                    }
                }
                updateVertex(e.second);
            }
            make_heap(open_list.begin(), open_list.end(), KeyCompare());
            computeShortestPath();
        }
        high_resolution_clock::time_point t1 = high_resolution_clock::now();
        cout << "Time elapsed for this round: " << (float)duration_cast<microseconds>(t1 - t0).count() / 1000.f << " ms" << endl;
    }
    high_resolution_clock::time_point tend = high_resolution_clock::now();

    computeShortestPath();
    //cout << graph << endl;
    cout << "Number of steps: " << nsteps << endl;
    cout << "Time elapsed: " << (float)duration_cast<microseconds>(tend - tstart).count() / 1000.f << " ms" << endl;

    if (graph.getNode(id_start)->rhs == 0) {
        cout << "Reached the goal!" << endl;
    } else {
        cout << "Could not reach the goal!" << endl;
    }

    return 0;
}
